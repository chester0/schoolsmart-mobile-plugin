/*global cordova, module*/

module.exports = {
    start: function (guardianId, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "LocationSync", "start", [guardianId]);
    },
    getId: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "FirebasePrimaryDeviceManager", "getId", []);
    },
    setThisDeviceAsPrimary: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "FirebasePrimaryDeviceManager", "setThisDeviceAsPrimary", []);
    },
};
