package com.pseudocoders;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by james on 14/09/16.
 */
@IgnoreExtraProperties
public class Coordinates {
    public String latitude;
    public String longitude;

    public Coordinates(){}

    public Coordinates(String latitude, String longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
