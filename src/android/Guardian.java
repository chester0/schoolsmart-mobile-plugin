package com.pseudocoders;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by james on 14/09/16.
 */
@IgnoreExtraProperties
public class Guardian {

    public GuardianLatestMessage latestMessage;
    public String primaryDeviceId;

    public Guardian() {}
}
